
let trainer = {
	name: "Alice",
	age: 19,
	pokemon: ["Pikachu", "Charizard", "Absol"],
	friends: {
		hoenn: ["May", "Max"],
		kanto: ["Brock", "Misty"],
	},
	talk: function() {
		console.log("Pikachu, I choose you!");
	}
};
console.log(trainer);

// ------------------------

console.log("Result of dot notation:");
console.log(trainer.name);

// ------------------------

console.log("Result of square bracket notation:");
console.log(trainer["pokemon"]);

// ------------------------

console.log("Result of talk method:");
trainer.talk();

// ------------------------

function pokemon(name, level, health, attack) {
	this.name = name;
	this.level = level;
	this.health = this.level * 2;
	this.attack = this.level;
};

let pokemon1 = new pokemon("Koffing", 10);
let pokemon2 = new pokemon("Rayquaza", 90);
let pokemon3 = new pokemon("Mew", 90);

console.log(pokemon1);
console.log(pokemon2);
console.log(pokemon3);

// ------------------------

pokemon.tackle = function(usedPokemon, targetPokemon) {
	console.log(usedPokemon.name + " attacked " + targetPokemon.name);
	let hpLeft = targetPokemon.health - usedPokemon.attack;

	let faint = function() {
		console.log(targetPokemon.name + " has fainted");
	};

	if(hpLeft <= 0) {
		console.log(faint());
	};
};

pokemon.tackle(pokemon3, pokemon1);